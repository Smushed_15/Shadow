package net.lax1dude.eaglercraft.v1_8;

public class EaglercraftVersion {
	
	
	//////////////////////////////////////////////////////////////////////
	
	/// Customize these to fit your fork:
	
	public static final String projectForkName = "";
	public static final String projectForkVersion = "";
	public static final String projectForkVendor = "";
	
	public static final String projectForkURL = "https://gitlab.com/lax1dude/eaglercraftx-1.8";
	
	//////////////////////////////////////////////////////////////////////
	
	
	
	// Do not change these, they must stay as credit to lax1dude's
	// original repository for maintaining the project:

	public static final String projectOriginName = "Shadow Client 3.0";
	public static final String projectOriginAuthor = "PeytonPlayz585";
	public static final String projectOriginRevision = "";
	public static final String projectOriginVersion = "";
	
	public static final String projectOriginURL = "https://github.com/PeytonPlayz595/Shadow-3.0";
	
	
	// Miscellaneous variables:

	public static final String mainMenuStringA = "";
	public static final String mainMenuStringB = projectOriginName + "" +
			projectOriginRevision + "" + projectOriginVersion + "";
	public static final String mainMenuStringC = "";
	public static final String mainMenuStringD = "EaglercraftX 1.8.8";

	public static final String mainMenuStringE = projectForkName + "" + projectForkVersion;
	public static final String mainMenuStringF = "";

	public static final String mainMenuStringG = "Shadow Client 3.0";
	public static final String mainMenuStringH = "Optifine + Shaders";

	public static final boolean mainMenuEnableGithubButton = false;

}
