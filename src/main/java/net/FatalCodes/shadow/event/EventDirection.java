package net.FatalCodes.shadow.event;

public enum EventDirection {
    OUTGOING,
    INCOMING;
}
